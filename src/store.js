import Vue from 'vue'
import Vuex from 'vuex'

import Axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    curNumPage: 1,
    users: [],
    user: null
  },
  mutations: {
    SET_CUR_NUM_PAGE(state, payload) {
      state.curNumPage = payload;
    },
    SET_USERS(state, payload) {
      state.users = payload;
    },
    SET_USER(state, payload) {
      state.user = payload;
    }
  },
  actions: {
    async FETCH_USERS(context) {
      const url = 'https://randomuser.me/api/?results=10&exc=location,registered,cell,nat'
      let {data} = await Axios.get(url);

      if (data.results && data.results.length > 0) {
        context.commit('SET_USERS', data.results);
      }
    },
  },
  getters: {
    CUR_NUM_PAGE(state) {
      return state.curNumPage;
    },
    USERS(state) {
      return state.users;
    },
    USER(state) {
      return state.user;
    }
  }
})
